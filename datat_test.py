import unittest
import os
import datat

class TestDatat(unittest.TestCase):
    def setUp(self):
        self.d = datat.Datat(["A","B","C"])
        self.d.append({"A":1, "B":"KJ"})
        
    def testbasics(self):
        assert len(self.d) == 1
        assert len(self.d.columns) == 3
        blank = self.d.blankdatum()
        assert len(blank) == 3
        assert all(name in blank for name in self.d.columns)
        
    def testiter(self):
        temp = next(iter(self.d))
        assert temp["A"] == 1
        assert temp["C"] == None
        for a, b, c in self.d.tuples:
            assert a == 1
            assert c == None
            break
        
    def testappend(self):
        assert len(self.d) == 1
        self.d.append({})
        assert len(self.d) == 2
        assert len(self.d[-1]) == 3
        
    def testcolumns(self):
        assert len(self.d.columns["A"]) == 1
        assert self.d.columns["A"][0] == 1
        assert not self.d.columns["C"][0]
        self.d.columns["C"] = [True]  # Check it doesn't throw an error
        
    def testtuples(self):
        self.d.tuples.append((4, "I", False))
        assert len(self.d.tuples) == 2
        a, b, c = self.d.tuples[-1]
        assert a == 4
        assert b == "I"
        assert c is False
        m = self.d.tuples[0:1]
        assert len(m) == 1
        assert m[0][1] == "KJ"
        
    def testfilter(self):
        self.d.tuples.append((3, "Look", False))
        self.d.tuples.append((6, "KJ", True))
        self.d.tuples.append((None, None, True))
        assert len(self.d) == 4
        assert len(self.d.filter(B="KJ")) == 2
        assert len(self.d.filter("B.startswith('Lo')")) == 1
        assert len(self.d.filter("A>2")) == 2, self.d #.filter(A=1)
        assert len(self.d.filter("A != 2", "not(C)")) == 2
        assert len(self.d.filter("C"))== 2
        
    def testCSV(self):
        testfile = "9876test.csv0"
        self.d.append({'A':True, 'B':False, 'C':1.0})
        self.d.save_csv(testfile)
        newd = datat.load_csv(testfile)
        os.unlink(testfile)
        assert len(newd) == 2
        assert len(newd.columns) == 3
        assert newd[0]["A"] == 1
        assert newd[0]["B"] == "KJ"
        assert newd[0]["C"] is None
        assert newd[1]["A"] is True
        assert newd[1]["B"] is False
        assert isinstance(newd[1]["C"], float)
        
    def testrepr(self):  # Just check that they don't throw an error.
        repr(self.d)
        repr(self.d.columns)
        repr(self.d.tuples)
        
    if datat.robjects:
        def testRtranslator(self):
            self.d.append({"A":2.0, "C": True})
            self.d.append({"B":"Boo", "C":False})
            df = self.d.translate_to_R()
            assert df.ncol == 3, df.ncol
            assert df.nrow == 3, df.nrow
            for n in df.names:
                assert n in ("A","B","C")
            bycol = dict(df.iteritems())
            assert isinstance(bycol["A"], datat.robjects.FloatVector)
            assert isinstance(bycol["B"], datat.robjects.FactorVector)
            assert type(bycol["C"]) == datat.robjects.Vector, bycol
            
class TestDatatNamedRows(unittest.TestCase):
    def setUp(self):
        self.d = datat.DatatNamedRows(["Q","W","E"])
        self.d["R"] = {"Q": True, "E":21}
        self.d["T"] = {"Q": False, "W": "Hi", "E": 45}
        
    def testbasics(self):
        assert len(self.d) == 2
        assert len(self.d.columns) == 3
        assert self.d["R"]["E"] == 21
        assert len(self.d["R"]) == 3
        self.d["T"]["W"] = "Bye"
        assert self.d["T"]["W"] == "Bye"
        
    def testblankdatum(self):
        bd = self.d.blankdatum()
        assert len(bd) == 3
        for k, v in bd.items():
            assert k in self.d.columns
            assert v is None
            
    def testiter(self):
        rname, row = next(iter(self.d.items()))
        assert len(row) == 3
        assert row["E"] in (21, 45) # Order can change, if OrderedDict not found
        for rname, (q, w, e) in self.d.tuples:
            assert rname in ("R", "T")
            if rname == "R":
                assert e == 21
            else:
                assert e == 45
                
    def testcolumns(self):
        assert len(self.d.columns) == 3
        assert self.d.columns["W"]["T"] == "Hi"
        self.d.columns["Q"] = {"R": False, "T": True}
        assert self.d["R"]["Q"] is False
        
    def testtuples(self):
        self.d.tuples["Y"] = (True, "Fish", 991)
        q, w, e = self.d.tuples["Y"]
        assert q is True
        assert w == "Fish"
        assert e == 991
        assert len(self.d.tuples) == 3
        
    def testfilter(self):
        self.d["Y"] = {"Q": True, "E":63, "W":"Hu"}
        assert set(self.d.filter(E=45).keys()) == set("T")
        assert set(self.d.filter("E<50").keys()) == set("TR")
        assert set(self.d.filter("W.startswith('H')").keys()) == set("YT")
    
    def testCSV(self):
        testfile = "2567test.csv0"
        self.d.save_csv(testfile)
        newd = datat.load_csv(testfile, namescol="(name)")
        os.unlink(testfile)
        assert len(newd) == 2
        assert len(newd.columns) == 3
        for colname in newd.columns:
            assert colname in self.d.columns
        assert newd["R"]["E"] == 21
        assert newd["T"]["Q"] is False
        
    def testrepr(self): # Just check they don't throw an error.
        repr(self.d)
        repr(self.d.columns)
        repr(self.d.tuples)
        
    if datat.robjects:
        def testRtranslator(self):
            df = self.d.translate_to_R()
            assert df.ncol == 3, df.ncol
            assert df.nrow == 2, df.nrow
            for n in df.names:
                assert n in ("Q","W","E")
            for n in df.rownames:
                assert n in ("R", "T")
            bycol = dict(df.iteritems())
            assert isinstance(bycol["E"], datat.robjects.IntVector)
            assert isinstance(bycol["W"], datat.robjects.FactorVector)
            assert type(bycol["Q"]) == datat.robjects.Vector, bycol
        
if __name__ == "__main__":
    unittest.main()
